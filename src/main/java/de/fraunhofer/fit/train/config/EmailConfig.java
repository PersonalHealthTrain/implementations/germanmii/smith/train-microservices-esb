package de.fraunhofer.fit.train.config;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

@Configuration
public class EmailConfig {
	
	@Autowired
	Environment env;


	
	
	@Bean
	public JavaMailSender getJavaMailSender() {
		JavaMailSenderImpl mailSender = new JavaMailSenderImpl();

		Properties prop = new Properties();
		prop.put("mail.smtp.auth", true);
		prop.put("mail.smtp.starttls.enable",true);
		prop.put("mail.smtp.host", env.getProperty("mail.smtp.host"));
		prop.put("mail.smtp.port", env.getProperty("mail.smtp.port"));
		prop.put("mail.debug",true);
		
		Session session = Session.getInstance(prop, new Authenticator() {
		    @Override
		    protected PasswordAuthentication getPasswordAuthentication() {
		        return new PasswordAuthentication(env.getProperty("mail.username"),env.getProperty("mail.password"));
		    }
		});
		mailSender.setSession(session);
		
		return mailSender;
	}

	@Bean
	public SimpleMailMessage emailTemplate() {
		SimpleMailMessage message = new SimpleMailMessage();
		message.setTo(env.getProperty("simple.message.to"));
		message.setFrom(env.getProperty("simple.message.from"));
		message.setText(env.getProperty("simple.message.msg"));

		return message;
	}

	@Bean
	public Session MailSession() {
		Properties prop = new Properties();
		prop.put("mail.smtp.auth", true);
		prop.put("mail.smtp.starttls.enable",env.getProperty("mail.smtp.auth"));
		prop.put("mail.smtp.host", env.getProperty("mail.smtp.host"));
		prop.put("mail.smtp.port", env.getProperty("mail.smtp.port"));
		prop.put("mail.smtp.ssl.trust", "smtp.mailtrap.io");
		
		Session session = Session.getInstance(prop, new Authenticator() {
		    @Override
		    protected PasswordAuthentication getPasswordAuthentication() {
		        return new PasswordAuthentication(env.getProperty("mail.username"),env.getProperty("mail.password"));
		    }
		});
		
		return session;
	}
}
