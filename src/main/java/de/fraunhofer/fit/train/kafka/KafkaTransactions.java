package de.fraunhofer.fit.train.kafka;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;

import de.fraunhofer.fit.train.facade.ServiceFacade;
import de.fraunhofer.fit.train.model_v1.Train;
import de.fraunhofer.fit.train.model_v1.result.IntegrationOutput;

@EnableKafka
@Component
public class KafkaTransactions {

    private static final Logger log = LoggerFactory.getLogger(KafkaTransactions.class);

    @Autowired
    private ServiceFacade facade;


    @KafkaListener(topics = "${topic.name}", containerFactory = "kafkaListenerContainerFactory")
    public void outputListener(IntegrationOutput output) throws Exception {

        log.info("Received message");

        log.info("!!!=====>>>  msg: "+output);
        

		if(output==null) {
        	return;
        }
		facade.saveTrainResult(output);
		Train train = facade.findTrainByInternalId(output.getTrainId());
		if(train==null) {
			throw new RuntimeException("There's no Train for input train id:" +output.getTrainId());
		}
		train.setResult(output);
		facade.sendMailMssgToAlertTheEndOfProcessment(output);
		
        Thread.sleep(2000);

        log.info("Processed message");
    }
    

}