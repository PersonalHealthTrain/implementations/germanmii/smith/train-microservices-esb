package de.fraunhofer.fit.train.model;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.stereotype.Repository;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Repository
@Document(collection = "phtUser")
public class PhtUser {
	
	@SerializedName("name")
	@Expose	
	private String name;
	
	@SerializedName("email")
	@Expose	
	private String email;
	
	@SerializedName("phonenumber")
	@Expose	
	private String phonenumber;
	
	@SerializedName("unifiedversion")
	@Expose	
	private String unifiedversion;
	
	@SerializedName("unifiedstate")
	@Expose	
	private String unifiedstate;

	
	@SerializedName("unifiedprefix")
	@Expose	
	private String unifiedprefix;
	
	
	@SerializedName("unifiedsuffix")
	@Expose	
	private String unifiedsuffix;
	
	@SerializedName("unifiedlandingpageurl")
	@Expose	
	private String unifiedlandingpageurl;
	

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhonenumber() {
		return phonenumber;
	}

	public void setPhonenumber(String phonenumber) {
		this.phonenumber = phonenumber;
	}

	public String getUnifiedversion() {
		return unifiedversion;
	}

	public void setUnifiedversion(String unifiedversion) {
		this.unifiedversion = unifiedversion;
	}

	public String getUnifiedstate() {
		return unifiedstate;
	}

	public void setUnifiedstate(String unifiedstate) {
		this.unifiedstate = unifiedstate;
	}

	public String getUnifiedprefix() {
		return unifiedprefix;
	}

	public void setUnifiedprefix(String unifiedprefix) {
		this.unifiedprefix = unifiedprefix;
	}

	public String getUnifiedsuffix() {
		return unifiedsuffix;
	}

	public void setUnifiedsuffix(String unifiedsuffix) {
		this.unifiedsuffix = unifiedsuffix;
	}

	public String getUnifiedlandingpageurl() {
		return unifiedlandingpageurl;
	}

	public void setUnifiedlandingpageurl(String unifiedlandingpageurl) {
		this.unifiedlandingpageurl = unifiedlandingpageurl;
	}
	
	

}
