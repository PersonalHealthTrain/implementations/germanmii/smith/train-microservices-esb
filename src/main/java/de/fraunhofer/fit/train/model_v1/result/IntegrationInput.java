package de.fraunhofer.fit.train.model_v1.result;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import de.fraunhofer.fit.train.model.PhtUser;

public class IntegrationInput {
	
	private String trainId;
	
	private String correlationId;
	
	@SerializedName("phtUser")
	@Expose	
	private PhtUser phtUser;
	

	public PhtUser getPhtUser() {
		return phtUser;
	}


	public void setPhtUser(PhtUser phtUser) {
		this.phtUser = phtUser;
	}

	private IntegrationInputWagon[] integrationInputWagon;

	public String getTrainId() {
		return trainId;
	}

	public void setTrainId(String trainId) {
		this.trainId = trainId;
	}

	public String getCorrelationId() {
		return correlationId;
	}

	public void setCorrelationId(String correlationId) {
		this.correlationId = correlationId;
	}

	public IntegrationInputWagon[] getIntegrationInputWagon() {
		return integrationInputWagon;
	}

	public void setIntegrationInputWagon(IntegrationInputWagon[] integrationInputWagon) {
		this.integrationInputWagon = integrationInputWagon;
	}
	
	

}
