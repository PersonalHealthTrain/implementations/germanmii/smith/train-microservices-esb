package de.fraunhofer.fit.train.model_v1.result;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.stereotype.Repository;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import de.fraunhofer.fit.train.model.PhtUser;
import de.fraunhofer.fit.train.model_v1.Train;

@Repository
@Document(collection = "phtResult")
public class IntegrationOutput {

	@SerializedName("trainId")
	@Expose
	private String trainId;

	@SerializedName("correlationId")
	@Expose
	private String correlationId;

	@SerializedName("phtUser")
	@Expose
	private PhtUser phtUser;

	@SerializedName("outputWagons")
	@Expose
	private IntegrationOutputWagon[] integrationOutputWagon;

	public PhtUser getPhtUser() {
		return phtUser;
	}

	public void setPhtUser(PhtUser phtUser) {
		this.phtUser = phtUser;
	}

	public String getTrainId() {
		return trainId;
	}

	public void setTrainId(String trainId) {
		this.trainId = trainId;
	}

	public String getCorrelationId() {
		return correlationId;
	}

	public void setCorrelationId(String correlationId) {
		this.correlationId = correlationId;
	}

	public IntegrationOutputWagon[] getIntegrationOutputWagon() {
		return integrationOutputWagon;
	}

	public void setIntegrationOutputWagon(IntegrationOutputWagon[] integrationOutputWagon) {
		this.integrationOutputWagon = integrationOutputWagon;
	}

}
