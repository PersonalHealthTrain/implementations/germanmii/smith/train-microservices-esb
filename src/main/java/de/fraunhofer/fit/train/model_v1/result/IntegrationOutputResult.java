package de.fraunhofer.fit.train.model_v1.result;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.stereotype.Repository;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


@Repository
@Document(collection = "outputWagonResults")
public class IntegrationOutputResult {
	
	
	@SerializedName("mimetype")
	@Expose	
	private String mimetype;
	
	
	@SerializedName("repositoryImageId")
	@Expose	
	private String repositoryImageId;
	
	
	@SerializedName("description")
	@Expose	
	private String description;
	
	
	@SerializedName("result")
	@Expose	
	private byte[] result;

	
	@SerializedName("statusCode")
	@Expose	
	private String statusCode;
	
	
	
	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getMimetype() {
		return mimetype;
	}

	public void setMimetype(String mimetype) {
		this.mimetype = mimetype;
	}

	public byte[] getResult() {
		return result;
	}

	public void setResult(byte[] result) {
		this.result = result;
	}

	public String getRepositoryImageId() {
		return repositoryImageId;
	}

	public void setRepositoryImageId(String repositoryImageId) {
		this.repositoryImageId = repositoryImageId;
	}
	
	

}
