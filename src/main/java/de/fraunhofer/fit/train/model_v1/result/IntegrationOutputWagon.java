package de.fraunhofer.fit.train.model_v1.result;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.stereotype.Repository;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Repository
@Document(collection = "outputWagons")
public class IntegrationOutputWagon {
	
	
	@SerializedName("wagonId")
	@Expose	
	private String wagonId;

	@SerializedName("outputWagonResults")
	@Expose	
	private IntegrationOutputResult[] integrationOutputResult;

	
	public String getWagonId() {
		return wagonId;
	}

	public void setWagonId(String wagonId) {
		this.wagonId = wagonId;
	}

	public IntegrationOutputResult[] getIntegrationOutputResult() {
		return integrationOutputResult;
	}

	public void setIntegrationOutputResult(IntegrationOutputResult[] integrationOutputResult) {
		this.integrationOutputResult = integrationOutputResult;
	}
	

	
}
