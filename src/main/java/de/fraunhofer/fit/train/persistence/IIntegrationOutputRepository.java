package de.fraunhofer.fit.train.persistence;

import java.util.List;

import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.QueryByExampleExecutor;

import de.fraunhofer.fit.train.model_v1.result.IntegrationOutput;

public interface IIntegrationOutputRepository  extends CrudRepository<IntegrationOutput, String>, QueryByExampleExecutor<IntegrationOutput> {

	@Query("{ '?0' : { $regex: ?1 } }")
	List<IntegrationOutput> findOneByQuery(String fieldStr,String cotent);

	@Query("{ ?0 : { $regex: ?1 } }")
	List<IntegrationOutput> findOneBySmampleQuery(String fieldStr,String cotent);
	
	@Query(value="{}", fields="{ '?0' : ?1}")
	List<IntegrationOutput> findOneBySimpleQuery(String fieldStr,String cotent);

	@Query("{ ?0 : { $regex: ?1 } }")
	List<IntegrationOutput> findOneByRegexQuery(String fieldStr, String cotent);
	
}
