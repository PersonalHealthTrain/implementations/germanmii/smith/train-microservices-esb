package de.fraunhofer.fit.train.properties;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties
public class ServiceLocatorEnvProperties {

	@Value("${srv_loc_protocol}")
	private String srv_loc_protocol;

	@Value("${srv_loc_host}")
	private String srv_loc_host;

	@Value("${srv_loc_port}")
	private String srv_loc_port;

	@Value("${srv_loc_app_ctx}")
	private String srv_loc_app_ctx;

	@Value("${srv_loc_rest_ctx}")
	private String srv_loc_rest_ctx;

	@Value("${app_env}")
	private String loadDb;
	
	@Value("${load_db}")
	private String appEnv;
	
	@Value("${env_db_name}")
	private String envDbName;
	
	@Value("${env_db_type}")
	private String envDbType;
	
	@Value("${env_db_token}")
	private String envDBToken;
	
	@Value("${env_ms_name}")
	private String envMsName;
	
	@Value("${env_ms_type}")
	private String envMsType;
	
	@Value("${env_ms_token}")
	private String envMsToken;
	
	@Value("${env_dav_name}")
	private String envDavName;
	
	@Value("${env_dav_type}")
	private String envDavType;
	
	@Value("${env_dav_token}")
	private String envDavToken;

	
	@Value("${esb_server_address}")
	private String esbServHost;
	
	@Value("${esb_server_port}")
	private String esbServerPort;
	
	
	@Value("${mongo_host}")
	private String esbDBHost;
	
	@Value("${mongo_port}")
	private String esbDBPort;


	@Value("${env_dav_metadata_name}")
	private String envDavMetadataName;
	
	@Value("${env_dav_metadata_type}")
	private String envDavMetadataType;
	
	@Value("${env_dav_metadata_token}")
	private String envDavMetadataToken;
	
	@Value("${env_dav_doc_name}")
	private String envDavDocumentationName;
	
	@Value("${env_dav_doc_type}")
	private String envDavDocumentationType;
	
	@Value("${env_dav_doc_token}")
	private String envDavDocumentationToken;
	
	@Value("${env_dav_page_name}")
	private String envDavPageName;
	
	@Value("${env_dav_page_type}")
	private String envDavPageType;
	
	@Value("${env_dav_page_token}")
	private String envDavPageToken;
	
	
	@Value("${pht_integration_protocol}")
	private String pht_integration_protocol;
	@Value("${pht_integration_host}")
	private String pht_integration_host;
	@Value("${pht_integration_port}")
	private String pht_integration_port;
	@Value("${pht_integration_app_ctx}")
	private String pht_integration_app_ctx;
	@Value("${pht_integration_rest_ctx}")
	private String pht_integration_rest_ctx;
	
	
	
	public String getPht_integration_protocol() {
		return pht_integration_protocol;
	}

	public void setPht_integration_protocol(String pht_integration_protocol) {
		this.pht_integration_protocol = pht_integration_protocol;
	}

	public String getPht_integration_host() {
		return pht_integration_host;
	}

	public void setPht_integration_host(String pht_integration_host) {
		this.pht_integration_host = pht_integration_host;
	}

	public String getPht_integration_port() {
		return pht_integration_port;
	}

	public void setPht_integration_port(String pht_integration_port) {
		this.pht_integration_port = pht_integration_port;
	}

	public String getPht_integration_app_ctx() {
		return pht_integration_app_ctx;
	}

	public void setPht_integration_app_ctx(String pht_integration_app_ctx) {
		this.pht_integration_app_ctx = pht_integration_app_ctx;
	}

	public String getPht_integration_rest_ctx() {
		return pht_integration_rest_ctx;
	}

	public void setPht_integration_rest_ctx(String pht_integration_rest_ctx) {
		this.pht_integration_rest_ctx = pht_integration_rest_ctx;
	}

	public String getEnvDavMetadataName() {
		return envDavMetadataName;
	}

	public void setEnvDavMetadataName(String envDavMetadataName) {
		this.envDavMetadataName = envDavMetadataName;
	}

	public String getEnvDavMetadataType() {
		return envDavMetadataType;
	}

	public void setEnvDavMetadataType(String envDavMetadataType) {
		this.envDavMetadataType = envDavMetadataType;
	}

	public String getEnvDavMetadataToken() {
		return envDavMetadataToken;
	}

	public void setEnvDavMetadataToken(String envDavMetadataToken) {
		this.envDavMetadataToken = envDavMetadataToken;
	}

	public String getEnvDavDocumentationName() {
		return envDavDocumentationName;
	}

	public void setEnvDavDocumentationName(String envDavDocumentationName) {
		this.envDavDocumentationName = envDavDocumentationName;
	}

	public String getEnvDavDocumentationType() {
		return envDavDocumentationType;
	}

	public void setEnvDavDocumentationType(String envDavDocumentationType) {
		this.envDavDocumentationType = envDavDocumentationType;
	}

	public String getEnvDavDocumentationToken() {
		return envDavDocumentationToken;
	}

	public void setEnvDavDocumentationToken(String envDavDocumentationToken) {
		this.envDavDocumentationToken = envDavDocumentationToken;
	}

	public String getEnvDavPageName() {
		return envDavPageName;
	}

	public void setEnvDavPageName(String envDavPageName) {
		this.envDavPageName = envDavPageName;
	}

	public String getEnvDavPageType() {
		return envDavPageType;
	}

	public void setEnvDavPageType(String envDavPageType) {
		this.envDavPageType = envDavPageType;
	}

	public String getEnvDavPageToken() {
		return envDavPageToken;
	}

	public void setEnvDavPageToken(String envDavPageToken) {
		this.envDavPageToken = envDavPageToken;
	}

	public String getEsbDBHost() {
		return esbDBHost;
	}

	public void setEsbDBHost(String esbDBHost) {
		this.esbDBHost = esbDBHost;
	}

	public String getEsbDBPort() {
		return esbDBPort;
	}

	public void setEsbDBPort(String esbDBPort) {
		this.esbDBPort = esbDBPort;
	}



	public String getEsbServHost() {
		return esbServHost;
	}

	public void setEsbServHost(String esbServHost) {
		this.esbServHost = esbServHost;
	}

	public String getEsbServerPort() {
		return esbServerPort;
	}

	public void setEsbServerPort(String esbServerPort) {
		this.esbServerPort = esbServerPort;
	}



	public String getSrv_loc_protocol() {
		return srv_loc_protocol;
	}

	public void setSrv_loc_protocol(String srv_loc_protocol) {
		this.srv_loc_protocol = srv_loc_protocol;
	}

	public String getSrv_loc_host() {
		return srv_loc_host;
	}

	public void setSrv_loc_host(String srv_loc_host) {
		this.srv_loc_host = srv_loc_host;
	}

	public String getSrv_loc_port() {
		return srv_loc_port;
	}

	public void setSrv_loc_port(String srv_loc_port) {
		this.srv_loc_port = srv_loc_port;
	}

	public String getSrv_loc_app_ctx() {
		return srv_loc_app_ctx;
	}

	public void setSrv_loc_app_ctx(String srv_loc_app_ctx) {
		this.srv_loc_app_ctx = srv_loc_app_ctx;
	}

	public String getSrv_loc_rest_ctx() {
		return srv_loc_rest_ctx;
	}

	public void setSrv_loc_rest_ctx(String srv_loc_rest_ctx) {
		this.srv_loc_rest_ctx = srv_loc_rest_ctx;
	}

	public String getLoadDb() {
		return loadDb;
	}

	public void setLoadDb(String loadDb) {
		this.loadDb = loadDb;
	}

	public String getAppEnv() {
		return appEnv;
	}

	public void setAppEnv(String appEnv) {
		this.appEnv = appEnv;
	}

	public String getEnvDbName() {
		return envDbName;
	}

	public void setEnvDbName(String envDbName) {
		this.envDbName = envDbName;
	}

	public String getEnvDbType() {
		return envDbType;
	}

	public void setEnvDbType(String envDbType) {
		this.envDbType = envDbType;
	}

	
	public String getEnvDBToken() {
		return envDBToken;
	}

	public void setEnvDBToken(String envDBToken) {
		this.envDBToken = envDBToken;
	}

	public String getEnvMsName() {
		return envMsName;
	}

	public void setEnvMsName(String envMsName) {
		this.envMsName = envMsName;
	}

	public String getEnvMsType() {
		return envMsType;
	}

	public void setEnvMsType(String envMsType) {
		this.envMsType = envMsType;
	}

	public String getEnvMsToken() {
		return envMsToken;
	}

	public void setEnvMsToken(String envMsToken) {
		this.envMsToken = envMsToken;
	}

	public String getEnvDavName() {
		return envDavName;
	}

	public void setEnvDavName(String envDavName) {
		this.envDavName = envDavName;
	}

	public String getEnvDavType() {
		return envDavType;
	}

	public void setEnvDavType(String envDavType) {
		this.envDavType = envDavType;
	}

	public String getEnvDavToken() {
		return envDavToken;
	}

	public void setEnvDavToken(String envDavToken) {
		this.envDavToken = envDavToken;
	}

	@PostConstruct
	public void print() {}

	@Bean
	public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
		return new PropertySourcesPlaceholderConfigurer();
	}

}
