package de.fraunhofer.fit.model;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.mongo.MongoDataAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.google.gson.Gson;

import de.fraunhofer.fit.train.config.KafkaConsumerConfig;
import de.fraunhofer.fit.train.facade.ServiceFacade;
import de.fraunhofer.fit.train.model.PhtUser;
import de.fraunhofer.fit.train.model_v1.Train;
import de.fraunhofer.fit.train.model_v1.Wagons;
import de.fraunhofer.fit.train.model_v1.result.IntegrationInput;
import de.fraunhofer.fit.train.model_v1.result.IntegrationInputProperties;
import de.fraunhofer.fit.train.model_v1.result.IntegrationInputWagon;
import de.fraunhofer.fit.train.model_v1.result.IntegrationOutput;
import de.fraunhofer.fit.train.model_v1.result.IntegrationOutputResult;
import de.fraunhofer.fit.train.model_v1.result.IntegrationOutputWagon;
import de.fraunhofer.fit.train.util.TrainUtil;

public class IntegrationModelTest {
	

	
	@Test
	public void simpleTest() throws IOException {
		
		

		
		String resultJson = "{\"trainId\":\"5dd3e2f0798f5d7b11cb963c\",\"correlationId\":\"5dd3e2f0798f5d7b11cb963c\",\"integrationOutputWagon\":[{\"integrationOutputResult\":[{\"mimetype\":\"text/double\",\"repositoryImageId\":\"427c8f4d-6646-45ef-9bb7-bd23dbf47319\",\"description\":\"count\",\"result\":[57,50,54,49,46,51,51,54,51,48,57,50,51,56,52],\"statusCode\":\"200\"}]}]}";
		Gson gson = new Gson();
		IntegrationOutput out = gson.fromJson(resultJson, IntegrationOutput.class);
		
		String content = TrainUtil.readFileToStr("/Users/jbjares/workspaces/PHTWorkspace/central_workspace_15112019/train-microservices-esb/src/main/resources/content/traintest.json");
		Train train = gson.fromJson(content, Train.class);
		
		
		
		for(IntegrationOutputWagon iwagon: out.getIntegrationOutputWagon()) {
			for(Wagons wagon:train.getWagons()) {
				if(iwagon.getWagonId().equals(wagon.getInternalWagonId())) {
					System.out.println("========> found: "+wagon.getName());
				}
			}
		}
	}
	
	@Ignore
	@Test
	public void parseOutputToJsonTest() {
		IntegrationOutput integrationOutput1 = new IntegrationOutput();
		integrationOutput1.setCorrelationId("06643hdv-dvrv344-frvrvrv-44598776");
		integrationOutput1.setTrainId("5d85a4deca90827e9e05fc6a");
		
		PhtUser user = new PhtUser();
		user.setEmail("jbjares@gmail.com");
		user.setName("Joao Bosco Jares");
		user.setPhonenumber("+491632361291");
		integrationOutput1.setPhtUser(user);
		
		List<IntegrationOutputWagon> integrationOutputWagonList = new ArrayList<IntegrationOutputWagon>();
		IntegrationOutputWagon integrationOutputWagon1 = new IntegrationOutputWagon();
		IntegrationOutputWagon integrationOutputWagon2 = new IntegrationOutputWagon();
		IntegrationOutputWagon integrationOutputWagon3 = new IntegrationOutputWagon();
		integrationOutputWagon1.setWagonId("3456789976545678");
		integrationOutputWagon2.setWagonId("3456789976545678");
		integrationOutputWagon3.setWagonId("3456789976545678");
		//integrationOutputWagon1.setIntegrationOutputResult(integrationOutputResult);
		List<IntegrationOutputResult> integrationOutputResult = new ArrayList<IntegrationOutputResult>();
		IntegrationOutputResult integrationOutputResult1 = new IntegrationOutputResult();
		IntegrationOutputResult integrationOutputResult2 = new IntegrationOutputResult();
		IntegrationOutputResult integrationOutputResult3 = new IntegrationOutputResult();
		integrationOutputResult1.setDescription("mean");
		integrationOutputResult1.setMimetype("double/text");
		integrationOutputResult1.setRepositoryImageId("byuiffgfgdbt54-ecvevevev-54666hj");
		integrationOutputResult1.setResult("20.76778".getBytes());
		integrationOutputResult1.setStatusCode("200");
		integrationOutputResult2.setDescription("total");
		integrationOutputResult2.setMimetype("double/text");
		integrationOutputResult2.setRepositoryImageId("343535-e32535cveve346vev-54646666hj64");
		integrationOutputResult2.setResult("407.6778,00".getBytes());
		integrationOutputResult2.setStatusCode("200");
		integrationOutputResult3.setStatusCode("500");
		integrationOutputResult3.setDescription("Internal Server Error");
		
		integrationOutputResult.add(integrationOutputResult1);
		integrationOutputResult.add(integrationOutputResult2);
		integrationOutputResult.add(integrationOutputResult3);
		
		integrationOutputWagon1.setIntegrationOutputResult(integrationOutputResult.toArray(new IntegrationOutputResult[integrationOutputResult.size()]));
		integrationOutputWagon2.setIntegrationOutputResult(integrationOutputResult.toArray(new IntegrationOutputResult[integrationOutputResult.size()]));
		integrationOutputWagon3.setIntegrationOutputResult(integrationOutputResult.toArray(new IntegrationOutputResult[integrationOutputResult.size()]));
		
		integrationOutputWagonList.add(integrationOutputWagon1);
		integrationOutputWagonList.add(integrationOutputWagon2);
		integrationOutputWagonList.add(integrationOutputWagon3);
		integrationOutput1.setIntegrationOutputWagon(integrationOutputWagonList.toArray(new IntegrationOutputWagon[integrationOutputWagonList.size()]));

		Gson gson = new Gson();
		String jsonFormat = gson.toJson(integrationOutput1);
		System.out.println(jsonFormat);
	}
	
	@Ignore
	@Test
	public void parseInputToJsonTest() {
		IntegrationInput input = new IntegrationInput();
		input.setCorrelationId("06643hdv-dvrv344-frvrvrv-44598776");
		input.setTrainId("5d85a4deca90827e9e05fc6a");
		
		PhtUser user = new PhtUser();
		user.setEmail("jbjares@gmail.com");
		user.setName("Joao Bosco Jares");
		user.setPhonenumber("+491632361291");
		input.setPhtUser(user);
		
		List<IntegrationInputWagon> integrationInputWagonList = new ArrayList<IntegrationInputWagon>();
		
		//==Wagon 1
		IntegrationInputWagon integrationInputWagon1 = new IntegrationInputWagon();
		integrationInputWagon1.setWagonId("5d85a4deca569586776e05fc6a");
		List<String> artifactsList = new ArrayList<String>();
		artifactsList.add("http://menzel.informatik.rwth-aachen.de:9999/5d80e888a13813ba54f7b154/CancerSqlQuery.sql");
		artifactsList.add("http://menzel.informatik.rwth-aachen.de:9999/5d80e888a13813ba54f7b154/DiabetesSparqlQuery.rq");
		artifactsList.add("http://menzel.informatik.rwth-aachen.de:9999/5d80e888a13813ba54f7b154/HepatitisFihrQuery.sh");
		integrationInputWagon1.setArtifacts(artifactsList.toArray(new String[artifactsList.size()]));
		List<IntegrationInputProperties> integrationInputPropertiesList = new ArrayList<IntegrationInputProperties>();
		IntegrationInputProperties integrationInputProperties1 = new IntegrationInputProperties();
		integrationInputProperties1.setKey("check_rare_disease");
		integrationInputProperties1.setValue("true");
		IntegrationInputProperties integrationInputProperties2 = new IntegrationInputProperties();
		integrationInputProperties2.setKey("disease_severity");
		integrationInputProperties2.setValue("high");
		IntegrationInputProperties integrationInputProperties3 = new IntegrationInputProperties();
		integrationInputProperties3.setKey("cut_value");
		integrationInputProperties3.setValue("10000");
		integrationInputPropertiesList.add(integrationInputProperties1);
		integrationInputPropertiesList.add(integrationInputProperties2);
		integrationInputPropertiesList.add(integrationInputProperties3);
		integrationInputWagon1.setProperties(integrationInputPropertiesList.toArray(new IntegrationInputProperties[integrationInputPropertiesList.size()]));
		List<String> stationsList = new ArrayList<String>();
		stationsList.add("stn_777");
		stationsList.add("stn_333");
		stationsList.add("stn_101");
		integrationInputWagon1.setStations(stationsList.toArray(new String[stationsList.size()]));
		integrationInputWagonList.add(integrationInputWagon1);
		
		//==Wagon 2
		IntegrationInputWagon integrationInputWagon2 = new IntegrationInputWagon();
		integrationInputWagon2.setWagonId("56659767980976087");
		artifactsList = new ArrayList<String>();
		artifactsList.add("http://menzel.informatik.rwth-aachen.de:9999/5d8cb2fac923bbde11e0a499/WedDavTestqQuery1.sql");
		artifactsList.add("http://menzel.informatik.rwth-aachen.de:9999/5d8cb2fac923bbde11e0a499/WedDavTestqQuery2.sql");
		artifactsList.add("http://menzel.informatik.rwth-aachen.de:9999/5d8cb2fac923bbde11e0a499/WedDavTestqQuery3.sql");
		artifactsList.add("http://menzel.informatik.rwth-aachen.de:9999/5d8cb2fac923bbde11e0a499/WedDavTestqQuery4.sql");
		artifactsList.add("http://menzel.informatik.rwth-aachen.de:9999/5d8cb2fac923bbde11e0a499/WedDavTestqQuery5.sql");

		integrationInputWagon2.setArtifacts(artifactsList.toArray(new String[artifactsList.size()]));
		integrationInputPropertiesList = new ArrayList<IntegrationInputProperties>();
		integrationInputProperties1 = new IntegrationInputProperties();
		integrationInputProperties1.setKey("aggregate");
		integrationInputProperties1.setValue("true");
		integrationInputProperties2 = new IntegrationInputProperties();
		integrationInputProperties2.setKey("calculate_mean");
		integrationInputProperties2.setValue("true");
		integrationInputProperties3 = new IntegrationInputProperties();
		integrationInputProperties3.setKey("mean_method");
		integrationInputProperties3.setValue("print(statistics.mean(${result_data}))");
		integrationInputPropertiesList.add(integrationInputProperties1);
		integrationInputPropertiesList.add(integrationInputProperties2);
		integrationInputPropertiesList.add(integrationInputProperties3);
		integrationInputWagon2.setProperties(integrationInputPropertiesList.toArray(new IntegrationInputProperties[integrationInputPropertiesList.size()]));
		stationsList = new ArrayList<String>();
		stationsList.add("stn_303");
		stationsList.add("stn_101");
		stationsList.add("stn_999");
		integrationInputWagon2.setStations(stationsList.toArray(new String[stationsList.size()]));
		integrationInputWagonList.add(integrationInputWagon2);
		input.setIntegrationInputWagon(integrationInputWagonList.toArray(new IntegrationInputWagon[integrationInputWagonList.size()]));
		
		Gson gson = new Gson();
		String jsonFormat = gson.toJson(input);
		System.out.println(jsonFormat);
		
	}

}
